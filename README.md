For Themed banners the user is able to choose from three set and most commonly used themes (Success, Error and Warning)

protocol Themed {
-     var theme: Theme
-     var bodyText: String
-     var roundedCornerRadius: Double
-     var animationDuration: Double
-     var hideDelay: Double
-     var hideButton: Bool

}



Customizable is used for Banners with that require more creative freedom, user is able to edit background color, icon, banner title and its color.

protocol Customizable {
-  var roundedCornerRadius: Double
-     var animationDuration: Double
-     var hideDelay: Double
-     var bodyText: String
-     var hideButton: Bool
-     var image: UIImage
-     var hideBanner: Bool
-     var backgroundColor: UIColor
-     var titleText: String
-     var titleColor: UIColor
-     var bodyColor: UIColor

}



